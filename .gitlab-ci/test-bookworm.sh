#!/bin/bash

cd $(dirname "$0")/..

export LC_ALL=C.UTF-8
export DEBIAN_FRONTEND=noninteractive
echo Etc/UTC > /etc/timezone
echo 'quiet "1";' \
     'APT::Get::Assume-Yes "true";' \
     'APT::Install-Recommends "0";' \
     'APT::Install-Suggests "0";' \
     'Acquire::Retries "20";' \
     'Dpkg::Use-Pty "0";' \
     'Debug::Acquire::gpgv "1";' \
     > /etc/apt/apt.conf.d/99gitlab

set -x
apt-get  update
apt-get dist-upgrade
apt-get install ansible ansible-lint git
ansible-galaxy install --force --role-file requirements.yml --roles-path .galaxy
# readable output from ansible
export ANSIBLE_LOAD_CALLBACK_PLUGINS=1
export ANSIBLE_STDOUT_CALLBACK=yaml

apt-get install iproute2 openssh-server sudo shellcheck

# the inventory with localhost is provided by the docker image
export ANSIBLE_STRATEGY=linear
ansible-playbook \
    --extra-vars "CI=$CI" \
    --verbose \
    debian-ipfs-setup.yml

set -e

test -e /usr/local/bin/ipfs

# run SSH extended test mode https://www.simplified.guide/ssh/test-config
test -d /run/sshd || mkdir /run/sshd
/usr/sbin/sshd -t

# check syntax of generated scripts, skip system provided cron jobs
shellcheck $(ls -1 /etc/cron.*ly/* | grep -vF -e /apt-compat -e /etckeeper -e /mdadm -e /tor)
